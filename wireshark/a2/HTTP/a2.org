#+TITLE: Wireshark Lab: HTTP v7.0
#+AUTHOR: *NAME:* Sil Vaes
#+OPTIONS: toc:nil
#+LATEX_CLASS_OPTIONS: [a4paper,11pt]
#+latex_header: \usepackage{libertine}
#+latex_header: \usepackage{libertinust1math}
#+latex_header: \usepackage[margin=0.5in]{geometry}
#+latex_header: \usepackage[scaled=0.95]{inconsolata}
#+latex_header: \usepackage[T1]{fontenc}

* The Basic HTTP GET/response interaction
1. Is your browser running HTTP version 1.0 or 1.1?  What version of HTTP is the server running?

*Answer:*
My browser is running HTTP version 1.1 and the server is also running HTTP version 1.1.

#+ATTR_LaTeX: :placement [!htpb]
#+caption: GET request
file:Q1.1.png


#+caption: OK reply
#+attr_latex: :width 300px :placement [!htpb]
file:Q1.2.png

2. [@2] What is the IP address of your computer?  Of the gaia.cs.umass.edu server?

*Answer:*
The local IP address of my computer is 192.169.0.178. The IP address of the =gaia.cs.umass.edu= server is 128.119.245.12.

#+ATTR_LaTeX: :placement [!htpb]
#+caption: IP addresses
[[file:Q2.png]]

3. [@3] What is the status code returned from the server to your browser?

*Answer:*
The status code is 200, OK.

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: Response code
[[file:Q3.png]]

\pagebreak
4. [@4] When was the HTML file that you are retrieving last modified at the server?

*Answer:*
The HTML file was last modified Saturday 5 october 2019 at 05:59:01 GMT.

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: Last modified
[[file:Q4.png]]

5. [@5] How many bytes of content are being returned to your browser?

*Answer:*
128 bytes of content are being returned. The total length is 538 bytes.

#+ATTR_LaTeX: :placement [!htpb] :width 200px
#+caption: Content bytes
[[file:Q5.png]]

6. [@6] By inspecting the raw data in the packet content window, do you see any headers within the data that are not displayed in the packet-listing window?  If so, name one.

*Answer:*
No, all data contained in the raw data is being displayed.

\pagebreak
* The HTTP CONDITIONAL GET/response interaction

Answer the following questions:

#+ATTR_LaTeX: :placement [!htpb]
#+caption: The HTTP communication
[[file:HTTP2.png]]

7. [@7]  Inspect the contents of the first HTTP GET request from your browser to the server.  Do you see an “IF-MODIFIED-SINCE” line in the HTTP GET?

*Answer:*
No, there is no "IF-MODIFIED-SINCE" line in the first HTTP GET.

#+ATTR_LaTeX: :placement [!htpb]
#+caption: First HTTP GET header
[[file:HTTP2Q7.png]]

8. [@8] Inspect the contents of the server response. Did the server explicitly return the contents of the file?   How can you tell?

*Answer:*
The server did explicitly return the contents of the file. In the header a "Content-Type" and "Content-Length" are present. The raw data also shows the returned HTML file contents.

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: Content type and length
[[file:HTTP2Q8.png]]

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: HTML contents
[[file:HTTP2Q8.1.png]]

\pagebreak
9. [@9] Now inspect the contents of the second HTTP GET request from your browser to the server.  Do you see an “IF-MODIFIED-SINCE:” line in the HTTP GET? If so, what information follows the “IF-MODIFIED-SINCE:” header?

*Answer:*
Yes, there is an "IF-MODIFIED-SINCE" header. It contains the timestamp when the file on the server was last modified, which was received from the last GET request.

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: If-Modified-Since header
[[file:HTTP2Q9.png]]

10. [@10] What is the HTTP status code and phrase returned from the server in response to this second HTTP GET?  Did the server explicitly return the contents of the file?   Explain.

*Answer:*
The response status code is 305 (Not Modified). The server did not explicitly return the file contents because "Last-Modified" date of the file on the server wasn't bigger than the date in "If-Modified-Since" header of the GET request.

There is also no "Content-Type" or "Content-Length" header present in the header section. There is also no HTML data present in the response, because the browser gets the data from cache.

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: Header contents
[[file:HTTP2Q10.png]]

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: Raw data contains no HTML data
[[file:HTTP2Q10.1.png]]


\pagebreak
* Retrieving Long Documents

11. [@11] How many HTTP GET request messages did your browser send?  Which packet number in the trace contains the GET message for the Bill or Rights?

*Answer:*
My browser only sent one HTTP GET request. Packet 51 contains the GET message for the Bill of Rights.

#+ATTR_LaTeX: :placement [!htpb]
#+caption: GET request
[[file:HTTP3Q1.png]]

12. [@12] Which packet number in the trace contains the status code and phrase associated with the response to the HTTP GET request?

*Answer:*
Packet number 54.

#+ATTR_LaTeX: :placement [!htpb] :width 300px
#+caption: OK response
[[file:HTTP3Q2.png]]


13. [@13] What is the status code and phrase in the response?

*Answer:*
The status code is 200, phrase OK. See the image in question 13.

14. [@14] How many data-containing TCP segments were needed to carry the single HTTP response and the text of the Bill of Rights?

*Answer:*
Only one TCP segment was needed to transfer the entire HTML page. The length of the TCP segment is 4861 bytes.

#+ATTR_LaTeX: :placement [!htpb]
#+caption: HTTP response
[[file:HTTP3Q3.png]]

\pagebreak
* HTML Documents with Embedded Objects

15. [@15] How many HTTP GET request messages did your browser send?  To which Internet addresses were these GET requests sent?

*Answer:*
My browser sent 3 GET requests. One for the HTML page, one for the logo and one for the cover. They were all sent to the same IP address, 128.119.245.12.

#+ATTR_LaTeX: :placement [!htpb]
#+caption: All GET requests and OK replies
[[file:HTTP4Q1.png]]

16. [@16] Can you tell whether your browser downloaded the two images serially, or whether they were downloaded from the two web sites in parallel?  Explain.

*Answer:*
They were downloaded serially. Because my browser requested the second image after the it receive the OK response for the first image. This can be seen in het previous image.

Counting from 20:32:39:00
   - GET for first image at 17 milliseconds.
   - OK for first image at 160 milliseconds.
   - GET for second image at 163 milliseconds.
   - OK for second image at 581 milliseconds.

Because the TCP ports are different, it means my browser opened a HTTP connection (with a GET request) to download the second image serially.
