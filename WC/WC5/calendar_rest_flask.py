#! /usr/bin/env python3

from flask import Flask, abort, request, Response
import calendar_rest_flask_help
import json

app = Flask(__name__)
# app.debug = True

CAL = calendar_rest_flask_help.Calendar()


@app.route("/appointments", methods=["POST"], strict_slashes=False)
def gen_appointment():
    xml_flag = is_xml(request)
    new_app = calendar_rest_flask_help.Appointment()
    try:
        if xml_flag == 1:
            new_app.from_xml(request.data)
        else:
            new_app.from_json(request.data)
    except json.decoder.JSONDecodeError:
        abort(400)
    app_id = calendar_rest_flask_help.id_generate()
    new_app.id = calendar_rest_flask_help.id_to_formatted_string(app_id)
    CAL.add_appointment(new_app.id, new_app)
    return calendar_rest_flask_help.id_to_formatted_string(app_id), 200


@app.route("/appointments", methods=["GET"], strict_slashes=False)
def get_appointments():
    query_args = request.args
    xml_flag = is_xml(request)

    start_time = calendar_rest_flask_help.json_deserializer_datetime_utc(
        query_args["start"]
    )
    stop_time = calendar_rest_flask_help.json_deserializer_datetime_utc(
        query_args["stop"]
    )
    # print(start_time)
    # print(stop_time)
    app_list = calendar_rest_flask_help.AppointmentList()
    start_stop_list = CAL.get_appointments_in_time_range(start_time, stop_time)
    if xml_flag == 1:
        return Response(app_list.to_xml(start_stop_list), mimetype="application/xml")

    return Response(app_list.to_json(start_stop_list), mimetype="application/json")


@app.route("/appointments/<id_appointment>", methods=["GET"])
def get_appointment_by_id(id_appointment):
    xml_flag = is_xml(request)
    appointment = CAL.get_appointment(id_appointment)
    try:
        if xml_flag == 1:
            return Response(appointment.to_xml(), mimetype="application/xml")
        return Response(appointment.to_json(), mimetype="application/json")
    except AttributeError:
        abort(404)


@app.route("/appointments/<id_appointment>", methods=["DELETE"])
def del_appointment_by_id(id_appointment):
    CAL.delete_appointment(id_appointment)
    return Response()


@app.route("/appointments/<id_appointment>", methods=["PUT"])
def update_appointment_by_id(id_appointment):
    xml_flag = is_xml(request)
    appointment = calendar_rest_flask_help.Appointment()
    if xml_flag == 1:
        appointment.from_xml(request.data)
    else:
        appointment.from_json(request.data)
    CAL.update_appointment(id_appointment, appointment)
    return Response()


def is_xml(req):
    xml_flag = 0
    try:
        if req.args["format"] == "xml":
            xml_flag = 1
    except:
        pass
    return xml_flag


if __name__ == "__main__":
    app.run()
