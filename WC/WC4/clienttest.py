#! /usr/bin/env python3

import time
import concurrent.futures
import requests
import http.client


HOST = "localhost"
PORT = 8888


def request_page():
    print("In fn")
    return requests.get("http://localhost:8888")

def clienttest():
    h1 = http.client.HTTPConnection(HOST, PORT)
    h1.request("GET", "/foo/bar")
    r1 = h1.getresponse()
    data1 = r1.read()
    print(data1)

with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
    futures = [executor.submit(request_page) for _ in range(16)]

    print("Executing total", len(futures), "jobs")

    for idx, future in enumerate(concurrent.futures.as_completed(futures, timeout=180.0)):
        res = future.result()  # This will also raise any exceptions
        print("Processed job", idx, "result", res)
