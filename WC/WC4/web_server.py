#! /usr/bin/env python3

import socket
import selectors
import types
import re
import datetime


HOST = "localhost"
PORT = 8888
BUFSIZE = 1024

REQUEST = re.compile(r"[A-Z]+ .* HTTP\/\d+[.]\d+\r\n")
EXT = re.compile(r"[.]\w+")

# Content types: image/jpeg, image/png, application/javascript, text/css, application/zip
CNT_TYPES = [
    b"Content-Type: image/jpeg; charset=UTF-8\r\n",
    b"Content-Type: image/png; charset=UTF-8\r\n",
    b"Content-Type: application/javascript; charset=UTF-8\r\n",
    b"Content-Type: text/css; charset=UTF-8\r\n",
    b"Content-Type: application/zip; charset=UTF-8\r\n",
    b"Content-Type: text/html; charset=UTF-8\r\n",
]

ALLOWED_EXT = [".jpg", ".png", ".js", ".css", ".zip", ".html"]

GENERAL_HEADER = (
    b"Connection: close\r\n" b"Cache-Control: no-cache\r\n" b"Allow: GET\r\n"
)

OK_HEADER = b"HTTP/1.1 200 OK\r\n"

NOT_FOUND_HEADER = (
    b"HTTP/1.1 404 Not Found\r\n" b"Content-Type: text/html; charset=UTF-8\r\n"
)

NOT_FOUND_HTML = b"<h1>404: Not found</h1>"

NOT_ALLOWED_HEADER = (
    b"HTTP/1.1 405 Method Not Allowed\r\n" b"Content-Type: text/html; charset=UTF-8\r\n"
)

NOT_ALLOWED_HTML = b"<h1>405: Method not allowed</h1>"

UNSUPPORTED_MEDIA_TYPE_HEADER = (
    b"HTTP/1.1 415 Unsupported Media Type\r\n"
    b"Content-Type: text/html; charset=UTF-8\r\n"
)

UNSUPPORTED_MEDIA_TYPE_HTML = b"<h1>415: Unsupported Media Type</h1>"

VERSION_NOT_SUPPORTED_HEADER = (
    b"HTTP/1.1 505 HTTP Version Not Supported\r\n"
    b"Content-Type: text/html; charset=UTF-8\r\n"
)
VERSION_NOT_SUPPORTED_HTML = b"<h1>505 HTTP Version Not Supported</h1>"


HTTP_MAJOR_VERSION = 1


# def parse_headers(data, request):
def parse_headers(data):
    # Match the HTTP request
    request_first = REQUEST.search(data.data_buffer).group(0)

    # Split the request to make parsing easier
    request_elem = request_first.split(" ")

    # Check HTTP version
    major_version = int(request_elem[2].replace("\r\n", "")[5:].split(".")[0])

    if major_version > HTTP_MAJOR_VERSION:
        data.out_data = construct_message(
            VERSION_NOT_SUPPORTED_HEADER, VERSION_NOT_SUPPORTED_HTML
        )
        return

    # If it is not a GET request, return method not allowed
    if request_elem[0] != "GET":
        data.out_data = construct_message(NOT_ALLOWED_HEADER, NOT_ALLOWED_HTML)
    else:
        # Ignore the leading '/' in the path of the request
        path = request_elem[1].strip("/")

        # If the request does not contain path, try to open de index.html in the root directory
        if path == "":
            try:
                with open("index.html", "rb") as file:
                    seg = file.read()
                    data.out_data = construct_message(OK_HEADER + CNT_TYPES[5], seg)
            except IOError:
                data.out_data = construct_message(NOT_FOUND_HEADER, NOT_FOUND_HTML)

        # Check if an explicit file is being requested
        elif "." in path:
            ext = EXT.search(path).group(0)
            # See if the file extension is allowed
            if not ext.lower() in ALLOWED_EXT:
                data.out_data = construct_message(
                    UNSUPPORTED_MEDIA_TYPE_HEADER, UNSUPPORTED_MEDIA_TYPE_HTML
                )
            else:
                try:
                    with open(path, "rb") as file:
                        seg = file.read()
                        data.out_data = construct_message(
                            OK_HEADER + CNT_TYPES[ALLOWED_EXT.index(ext)], seg
                        )
                except IOError:
                    data.out_data = construct_message(NOT_FOUND_HEADER, NOT_FOUND_HTML)
        else:
            # If it is not an explicit file, try to find the index.html. If no index.html is present, return 'not found'
            try:
                with open(path + "/index.html", "rb") as file:
                    seg = file.read()
                    data.out_data = construct_message(OK_HEADER + CNT_TYPES[5], seg)
            except IOError:
                data.out_data = construct_message(NOT_FOUND_HEADER, NOT_FOUND_HTML)


def handle_connection(socket_conn, selector_conn):
    conn_f, addr = socket_conn.accept()
    print(f"Connection in {addr} accepted")
    conn_f.setblocking(False)
    conn_data = types.SimpleNamespace(
        addr=addr, in_data=b"", out_data=b"", data_buffer=""
    )
    selector_conn.register(
        conn_f, (selectors.EVENT_READ | selectors.EVENT_WRITE), data=conn_data
    )


def handle_request(s_key, s_mask, selector_conn):
    sock = s_key.fileobj
    data = s_key.data
    if s_mask & selectors.EVENT_READ:
        request_data = sock.recv(BUFSIZE).decode("utf-8")
        data.data_buffer += request_data
        if not request_data:
            print(f"Closing connection {data.addr}")
            selector_conn.unregister(sock)
            sock.close()
        if data.data_buffer.endswith("\r\n\r\n"):
            parse_headers(data)
            data.data_buffer = ""
    if s_mask & selectors.EVENT_WRITE:
        if data.out_data:
            try:
                sock.sendall(data.out_data)
                data.out_data = ""
            except OSError:
                print("Failed to send data.")
                print(f"Closing connection {data.addr}")
                selector_conn.unregister(sock)
                sock.close()
                return
            print(f"Closing connection {data.addr}")
            selector_conn.unregister(sock)
            sock.close()


def generate_date(timestamp):
    weekday = [b"Mon", b"Tue", b"Wed", b"Thu", b"Fri", b"Sat", b"Sun"][
        timestamp.weekday()
    ]
    month = [
        b"Jan",
        b"Feb",
        b"Mar",
        b"Apr",
        b"May",
        b"Jun",
        b"Jul",
        b"Aug",
        b"Sep",
        b"Oct",
        b"Nov",
        b"Dec",
    ][timestamp.month - 1]
    return b"%s, %02d %s %04d %02d:%02d:%02d GMT" % (
        weekday,
        timestamp.day,
        month,
        timestamp.year,
        timestamp.hour,
        timestamp.minute,
        timestamp.second,
    )


def construct_message(header, contents):
    return (
        header
        + GENERAL_HEADER
        + b"Date: %s\r\n" % generate_date(datetime.datetime.now())
        + b"Content-Length: %d\r\n\r\n" % len(contents)
        + contents
    )


if __name__ == "__main__":
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_sock:
            server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server_sock.setblocking(False)
            server_sock.bind((HOST, PORT))
            server_sock.listen()
            print(f"Opened socket on {HOST}:{PORT}")

            with selectors.DefaultSelector() as selector:
                selector.register(server_sock, selectors.EVENT_READ, data=None)
                while server_sock:
                    EVENTS = selector.select(timeout=None)
                    for key, mask in EVENTS:
                        if key.data is None:
                            handle_connection(key.fileobj, selector)
                        else:
                            handle_request(key, mask, selector)

    except KeyboardInterrupt:
        print("Exiting...")
    except OSError:
        print("Socket error")
