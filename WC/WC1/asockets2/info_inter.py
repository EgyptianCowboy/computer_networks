#! /usr/bin/env python3

# * Preamble
import socket
import time
import sys

HOST = "127.0.0.1"
PORT = 2000
TIMEOUT = 30


# * Functions
def setup_server(host, port):
    """
    Function to set up the server
    """
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except OSError:
        server_socket = None
    try:
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((host, port))
        server_socket.listen()
    except OSError:
        server_socket.close()
        server_socket = None

    if server_socket is None:
        print("Could not create socket")
        sys.exit(1)

    return server_socket


def parse_commands(cmd, addr):
    """
    Function to parse incoming commands
    """
    data = ""
    exitcode = 0
    if "IP" in cmd:
        data = str(addr[0])
    elif cmd == "TIME":
        data = str(time.ctime())
    elif cmd == "EXIT":
        data = "CLOSING SERVER"
        exitcode = 1
    else:
        data = "UNKNOWN COMMAND"
    return data, exitcode


# * Main
def main():
    """
    Main function
    """
    server_socket: socket = setup_server(HOST, PORT)
    reply = ""
    exitcode = 0
    indata: str = ""
    try:
        while not exitcode:
            conn, addr = server_socket.accept()
            conn.settimeout(TIMEOUT)
            while not exitcode:
                try:
                    indata += conn.recv(1).decode("utf-8")
                except socket.timeout:
                    print("Timeout")
                    conn.close()
                    break
                except OSError:
                    print("Could not receive data")
                    conn.close()
                    break

                if not indata:
                    print("Lost connection")
                    conn.close()
                    break

                if "\n" in indata:
                    try:
                        reply, exitcode = parse_commands(
                            indata.replace("\n", "").replace("\r", ""), addr
                        )
                    except AttributeError:
                        print("Not a string")

                    indata = ""
                    try:
                        conn.sendall(reply.encode("utf-8"))
                    except OSError:
                        print("Could not send data.")
                        conn.close()
                        break

        print("CLOSING SERVER")
    except KeyboardInterrupt:
        print("Exiting...")

    server_socket.shutdown(socket.SHUT_WR)
    server_socket.close()


if __name__ == "__main__":
    main()
