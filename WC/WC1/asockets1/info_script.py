#! /usr/bin/env python3

# * Preamble
import socket
import time
import sys

HOST = '127.0.0.1'
PORT = 2000


# * Functions
def setup_server(host, port):
    """
    Function to set up the server.
    """
    try:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except OSError:
        server_socket = None
    try:
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((host, port))
        server_socket.listen()
    except OSError:
        print("Could not bind socket.")
        server_socket.close()
        server_socket = None

    if server_socket is None:
        print("Could not create socket")
        sys.exit(1)

    return server_socket


def parse_commands(cmd, addr):
    """
    Function to parse incoming commands.
    """
    data = ""
    exitcode = 0
    if cmd == "IP":
        data = str(addr[0])
    elif cmd == "TIME":
        data = str(time.ctime())
    elif cmd == "EXIT":
        exitcode = 1
    else:
        data = "UNKNOWN COMMAND"
    return data, exitcode


# * Main
def main():
    """
    Main function
    """
    server_socket = setup_server(HOST, PORT)
    data = ""
    exitcode = 0
    indata = ""
    try:
        while not exitcode:
            conn, addr = server_socket.accept()
            while not exitcode:
                try:
                    indata = conn.recv(1024)
                except OSError:
                    print("Could not receive data")
                    conn.close()
                    break

                if not indata:
                    print("Connection lost")
                    break

                data, exitcode = parse_commands(indata.decode('utf-8'), addr)
                try:
                    conn.sendall(data.encode('utf-8'))
                except OSError:
                    print("Failed to send data")
                    conn.close()
                    break

                if exitcode:
                    print("SHUTTING DOWN SERVER")

    except KeyboardInterrupt:
        print("Exiting...")

    server_socket.shutdown(socket.SHUT_RDWR)
    server_socket.close()


if __name__ == "__main__":
    main()
