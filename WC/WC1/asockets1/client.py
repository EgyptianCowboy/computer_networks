#! /usr/bin/env python3
import socket
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("127.0.0.1", 2000))
print("sending")
string = "TIME\n"
s.sendall(string.encode("utf8"))
msg = s.recv(1024)
print(msg.decode("utf-8"))
s.sendall(b"IP\n")
msg = s.recv(1024)
print(msg.decode("utf-8"))

s.close()
time.sleep(3)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("localhost", 2000))

s.sendall(b"asad\n")
msg = s.recv(1024)
print(msg.decode("utf-8"))
s.sendall(b"TIME\n")
msg = s.recv(1024)
print(msg.decode("utf-8"))
s.sendall(b"IP\n")
msg = s.recv(1024)
print(msg.decode("utf-8"))

s.sendall(b"EXIT\n")
s.close()
