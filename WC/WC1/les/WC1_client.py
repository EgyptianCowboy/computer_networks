#!/usr/bin/env python3
import socket

HOST = '127.0.0.1'
PORT = 24001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.sendall(b'Hmm')
data = s.recv(1024)
s.close()

print("Received:", data.decode("utf-8"))
