#!/usr/bin/env python3

# * Preamble
import socket

# * Program
HOST = '127.0.0.1'
PORT = 24001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# => <socket.socket fd=3, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('0.0.0.0', 0)>
s.bind((HOST, PORT))
s.listen()
conn, addr = s.accept()

while True:
    data = conn.recv(1024)
    print("Received:",  data.decode("utf-8"))
    if not data:
        break
    conn.sendall(data)
