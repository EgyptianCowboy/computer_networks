#! /usr/bin/python3

import socket
import struct
import concurrent.futures
from parse_dns_name import parse_dns_name
from parse_hosts_file import parse_hosts_file
import ctypes
from collections import OrderedDict
import sys
import ipaddress

DNS_SERV_IP = "8.8.8.8"
DNS_SERV_PORT = 53

DNS_PROXY_IP = "127.0.0.1"
DNS_PROXY_PORT = 53

DNS_HEADER = struct.Struct("!6H")
DNS_Q_TYPE_CLASS = struct.Struct("!2H")
DNS_RESP = struct.Struct("!2HIH")
DNS_DOMAIN_PTR = struct.Struct("!2B")

DNS_IPV4 = struct.Struct("!I")
DNS_IPV6 = struct.Struct("!2Q")

# ALLOWED_DNS_TYPES = [1, 5, 28]

RUN = 1

TRANSIT_REQ = {}

KNOWN_HOSTS = OrderedDict()
CACHE_LEN = 0
FILE_HOSTS = {}


def get_flags(flags):
    flag_dict = {}
    flag_dict["qr"] = (flags & 0x8000) >> 15
    flag_dict["opcode"] = (flags & 0x7800) >> 11
    flag_dict["aa"] = (flags & 0x0400) >> 10
    flag_dict["tc"] = (flags & 0x0200) >> 9
    flag_dict["rd"] = (flags & 0x0100) >> 8
    flag_dict["ra"] = (flags & 0x0080) >> 7
    flag_dict["z"] = (flags & 0x0040) >> 6
    flag_dict["ad"] = (flags & 0x0020) >> 5
    flag_dict["cd"] = (flags & 0x0010) >> 4
    flag_dict["rcode"] = flags & 0x000F
    return flag_dict


def external_dns():
    while RUN == 1:
        try:
            data, addr = DNS_SERV_SOCK.recvfrom(1024)
        except KeyboardInterrupt:
            break
        if addr == (DNS_SERV_IP, DNS_SERV_PORT):
            try:
                (
                    answer_id,
                    flags,
                    qcount,
                    ancount,
                    nscount,
                    arcount,
                ) = DNS_HEADER.unpack_from(data)
            except:
                print("Couldn't unpack response data")
            if answer_id in TRANSIT_REQ.keys():
                total_offset = DNS_HEADER.size
                domain_response, domain_enc, domain_offset = parse_dns_name(
                    data[total_offset:]
                )
                # print(domain_response)
                total_offset += domain_offset + DNS_Q_TYPE_CLASS.size
                # print(ancount)
                send_flag = 1
                for _ in range(ancount):
                    send_flag = 1
                    domain, domain_enc, domain_offset = parse_dns_name(
                        data, total_offset
                    )
                    total_offset += domain_offset
                    (dns_type, dns_class, ttl, rdlen) = DNS_RESP.unpack_from(
                        data, total_offset
                    )
                    total_offset += DNS_RESP.size
                    if dns_type == 1:
                        (resp,) = DNS_IPV4.unpack_from(data, total_offset)
                        total_offset += DNS_IPV4.size
                    elif dns_type == 5:
                        cname, resp, cname_offset = parse_dns_name(data, total_offset)
                        total_offset += cname_offset
                    elif dns_type == 28:
                        (resp1, resp2,) = DNS_IPV6.unpack_from(data, total_offset)
                        resp = [resp1, resp2]
                        # print(hex(resp))
                        # print(ipaddress.IPv6Address(resp).compressed)
                        total_offset += DNS_IPV6.size
                    else:
                        print("Invalid DNS type")
                        response_data = b""
                        offset = 0
                        flags = flags | 0x8004
                        qcount = 0
                        ancount = 0
                        arcount = 0
                        resp_header = struct.pack(
                            "!6H", answer_id, flags, qcount, ancount, nscount, arcount,
                        )
                        offset += DNS_HEADER.size
                        name = b""
                        name += domain_enc + data[total_offset : total_offset + 4]
                        offset += domain_offset
                        response_data = resp_header
                        DNS_PROXY_SOCK.sendto(response_data, TRANSIT_REQ[answer_id])
                        send_flag = 0
                        break
                    KNOWN_HOSTS[f"{domain}_{dns_type}"] = [
                        dns_type,
                        dns_class,
                        ttl,
                        rdlen,
                        resp,
                    ]
                    if len(KNOWN_HOSTS) > CACHE_LEN:
                        popitem = KNOWN_HOSTS.popitem(last=False)
                        print(f"Popped: {popitem}")
                if send_flag == 1:
                    DNS_PROXY_SOCK.sendto(data, TRANSIT_REQ[answer_id])
                del TRANSIT_REQ[answer_id]


def internal_dns():
    while RUN == 1:
        try:
            data, addr = DNS_PROXY_SOCK.recvfrom(1024)  # buffer size is 1024 bytes
        except KeyError:
            break
        try:
            (
                query_id,
                flags,
                qcount,
                ancount,
                nscount,
                arcount,
            ) = DNS_HEADER.unpack_from(data)
        except:
            print("Couldn't unpack query data")

        print(KNOWN_HOSTS)
        questions = data[DNS_HEADER.size :]

        domain_string, domain_encoded, domain_bytes = parse_dns_name(questions)
        # total_offset = DNS_HEADER.size + domain_bytes
        qtype, qclass, = DNS_Q_TYPE_CLASS.unpack_from(
            data, DNS_HEADER.size + domain_bytes
        )
        key_string = f"{domain_string}_{qtype}"
        file_addr_ok = 0
        if domain_string in FILE_HOSTS.keys():
            file_addr_ok = in_hostfile(
                domain_string,
                domain_bytes,
                domain_encoded,
                query_id,
                flags,
                qcount,
                ancount,
                nscount,
                arcount,
                addr,
                qtype,
            )
        if key_string in KNOWN_HOSTS.keys() and file_addr_ok == 0:
            response_data = ctypes.create_string_buffer(
                DNS_HEADER.size
                + domain_bytes
                + DNS_Q_TYPE_CLASS.size
                + DNS_DOMAIN_PTR.size
                + DNS_RESP.size
                + KNOWN_HOSTS[key_string][3]
            )
            offset = 0
            flags = flags | 0x8000
            flags = flags & 0x8100
            # flags = 0x8080
            ancount = 1
            arcount = 0
            DNS_HEADER.pack_into(
                response_data,
                offset,
                query_id,
                flags,
                qcount,
                ancount,
                nscount,
                arcount,
            )
            offset += DNS_HEADER.size
            domain_pos = offset
            struct.pack_into(
                f"!{domain_bytes}B", response_data, offset, *domain_encoded
            )
            offset += domain_bytes
            DNS_Q_TYPE_CLASS.pack_into(response_data, offset, qtype, qclass)
            offset += DNS_Q_TYPE_CLASS.size

            DNS_DOMAIN_PTR.pack_into(response_data, offset, 0xC0, domain_pos)
            offset += DNS_DOMAIN_PTR.size

            DNS_RESP.pack_into(
                response_data,
                offset,
                KNOWN_HOSTS[key_string][0],
                KNOWN_HOSTS[key_string][1],
                KNOWN_HOSTS[key_string][2],
                KNOWN_HOSTS[key_string][3],
            )
            offset += DNS_RESP.size
            if qtype == 1:
                DNS_IPV4.pack_into(response_data, offset, KNOWN_HOSTS[key_string][4])
            elif qtype == 28:
                DNS_IPV6.pack_into(response_data, offset, *KNOWN_HOSTS[key_string][4])
            elif qtype == 5:
                struct.pack_into(
                    f"{KNOWN_HOSTS[key_string][3]}B",
                    response_data,
                    offset,
                    *KNOWN_HOSTS[key_string][4],
                )
            offset += KNOWN_HOSTS[key_string][3]
            # print(response_data.raw)
            DNS_PROXY_SOCK.sendto(response_data, addr)
        else:
            TRANSIT_REQ[query_id] = addr
            DNS_SERV_SOCK.sendto(data, (DNS_SERV_IP, DNS_SERV_PORT))


def in_hostfile(
    domain_string,
    domain_bytes,
    domain_encoded,
    query_id,
    flags,
    qcount,
    ancount,
    nscount,
    arcount,
    addr,
    dns_type,
):
    ip = ipaddress.ip_address(FILE_HOSTS[domain_string])

    if (ip.version == 4) and (dns_type != 1):
        print("File address type and query mismatch")
        return 0

    if (ip.version == 6) and (dns_type != 28):
        print("File address type and query mismatch")
        return 0

    print("Redirecting")
    response_data = ctypes.create_string_buffer(
        DNS_HEADER.size
        + domain_bytes
        + DNS_Q_TYPE_CLASS.size
        + DNS_DOMAIN_PTR.size
        + DNS_RESP.size
    )
    offset = 0
    # flags = flags | 0x8000
    flags = 0x8000
    ancount = 1
    arcount = 0
    DNS_HEADER.pack_into(
        response_data, offset, query_id, flags, qcount, ancount, nscount, arcount,
    )
    offset += DNS_HEADER.size
    domain_pos = offset
    struct.pack_into(f"!{domain_bytes}B", response_data, offset, *domain_encoded)
    offset += domain_bytes
    if ip.version == 4:
        DNS_Q_TYPE_CLASS.pack_into(response_data, offset, 1, 1)
        offset += DNS_Q_TYPE_CLASS.size
        DNS_DOMAIN_PTR.pack_into(response_data, offset, 0xC0, domain_pos)
        offset += DNS_DOMAIN_PTR.size

        DNS_RESP.pack_into(
            response_data, offset, 1, 1, 0, DNS_IPV4.size,
        )
        offset += DNS_RESP.size

        addr_buff = ctypes.create_string_buffer(DNS_IPV4.size)
        DNS_IPV4.pack_into(addr_buff, 0, int(ipaddress.IPv4Address(ip.exploded)))
        offset += DNS_IPV4.size
        response_complete = response_data.raw + addr_buff.raw
        # print(response_complete)
        DNS_PROXY_SOCK.sendto(response_complete, addr)
    elif ip.version == 6:
        DNS_Q_TYPE_CLASS.pack_into(response_data, offset, 28, 1)
        offset += DNS_Q_TYPE_CLASS.size
        DNS_DOMAIN_PTR.pack_into(response_data, offset, 0xC0, domain_pos)
        offset += DNS_DOMAIN_PTR.size
        DNS_RESP.pack_into(
            response_data, offset, 28, 1, 0, DNS_IPV6.size,
        )
        offset += DNS_RESP.size
        addr_buff = ctypes.create_string_buffer(DNS_IPV6.size)

        ip_expl = int(ipaddress.IPv6Address(ip.exploded))
        ip_list = [
            ((ip_expl >> 64) & 0xFFFFFFFFFFFFFFFF),
            (ip_expl & 0xFFFFFFFFFFFFFFFF),
        ]
        # print(ip_list)
        DNS_IPV6.pack_into(addr_buff, 0, *ip_list)
        offset += DNS_IPV6.size
        response_complete = response_data.raw + addr_buff.raw
        # print(response_complete)
        DNS_PROXY_SOCK.sendto(response_complete, addr)
    return 1


def setup_sever():
    """
        Function to setup server
    """
    run = 1
    try:
        proxy_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        serv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
    except OSError:
        proxy_sock = None
        serv_sock = None
    try:
        proxy_sock.bind((DNS_PROXY_IP, DNS_PROXY_PORT))
        serv_sock.bind(("", 5006))
    except OSError:
        proxy_sock = None
        serv_sock = None
        proxy_sock.close()
        serv_sock.close()

    if proxy_sock is None or serv_sock is None:
        print("Could not create socket")
        run = 0

    return proxy_sock, serv_sock, run


if __name__ == "__main__":
    DNS_PROXY_SOCK, DNS_SERV_SOCK, RUN = setup_sever()
    FILE_HOSTS = parse_hosts_file("hosts")
    print(FILE_HOSTS)
    CACHE_LEN = 500
    try:
        if len(sys.argv) > 1:
            CACHE_LEN = int(sys.argv[1])
    except ValueError:
        print("Argument has to be an integer")
        RUN = 0

    try:
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
            future_int_dns = executor.submit(internal_dns)
            future_ext_dns = executor.submit(external_dns)
        # internal_dns()
    except KeyboardInterrupt:
        RUN = 0
