#!/usr/bin/python

import argparse
import socket
import sys
import concurrent.futures
import time
import os

RUN = 1
BUFF_SIZE = 1024

INPUT_DICT = {
    "ls": "LIST",
    "put": "STOR",
    "get": "RETR",
    "quit": "QUIT",
    "bye": "QUIT",
    "del": "DELE",
}

ASCII_EXT = [
    ".txt",
    ".log",
    ".c",
    ".cpp",
    ".h",
    ".py",
    ".scm",
    ".lisp",
    ".rs",
    ".html",
    ".css",
    ".readme",
    ".org",
]

DOWNLOAD_DIR = "downloads"


def parse_args():
    """
    Parse ftp client arguments
    """
    parser = argparse.ArgumentParser(description="FTP client.")
    parser.add_argument("host_port", type=str)
    parser.add_argument("username", type=str)
    parser.add_argument("password", type=str)
    return parser.parse_args()


def setup_tcp(host_port):
    """
    Function to set up the TCP connection
    """
    try:
        comm_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except OSError:
        comm_sock = None

    try:
        comm_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # comm_sock.bind((host_port[0], int(host_port[1])))
        comm_sock.connect((host_port[0], int(host_port[1])))
        # comm_sock.listen()
    except OSError:
        comm_sock.close()
        comm_sock = None
    except TypeError:
        print("Port is not an integer")

    if comm_sock is None:
        print("Could not create socket")
        sys.exit(1)

    print(comm_sock.getpeername())
    print(comm_sock.getsockname())
    return comm_sock


def construct_port(port_int):
    port_list = [str(port_int // 256), str(port_int % 256)]
    return ",".join(port_list)


def send_file(commsock, datasock):
    print("Send file")


def recv_file(commsock, datasock):
    print("Retrieve file")


def get_file_ext(file_string):
    _, file_ext = os.path.splitext(file_string)
    if file_ext == "":
        raise
    return file_ext


def create_datasock(comm_sock):
    try:
        data_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except OSError:
        data_sock = None

    try:
        data_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    except OSError:
        data_sock.close()
        data_sock = None
    if data_sock is None:
        print("Could not create socket")
        sys.exit(1)

    data_sock.bind((comm_sock.getsockname()[0], comm_sock.getsockname()[1] + 1))
    data_sock.listen()
    return data_sock


def comm_func(commsock, args):
    commsock.sendall(f"USER {args.username}\r\n".encode("ascii"))
    print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())
    commsock.sendall(f"PASS {args.password}\r\n".encode("ascii"))
    print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())
    global RUN
    while RUN == 1:
        i_val = input("ftp > ").split()
        try:
            ftp_msg = INPUT_DICT[i_val[0]]
            if ftp_msg == "QUIT":
                commsock.sendall(f"{ftp_msg}\r\n".encode("ascii"))
                data = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                print(data)
                RUN = 0
                continue
            # print(ftp_msg)
            elif ftp_msg == "LIST":
                datasock = create_datasock(commsock)
                port_str = construct_port(datasock.getsockname()[1])
                port_cmd = (
                    f"PORT {datasock.getsockname()[0].replace('.', ',')},{port_str}\r\n"
                )
                commsock.sendall(port_cmd.encode("ascii"))
                print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())

                commsock.sendall(f"{ftp_msg}\r\n".encode("ascii"))
                dataconn, dataddr = datasock.accept()
                data = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                print(data)
                split_data = data.split()
                while split_data[0] != "226":
                    if split_data[0] != "226":
                        ls_data = dataconn.recv(BUFF_SIZE).decode("ascii").rstrip()
                        print(ls_data)
                    data = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                    print(data)
                    split_data = data.split()
                dataconn.close()
                datasock.close()
            elif ftp_msg == "DELE":
                commsock.sendall(f"{ftp_msg} {i_val[1]}\r\n".encode("ascii"))
                data = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                print(data)
            elif ftp_msg == "STOR":
                print("Storing file")
                try:
                    file_ext = get_file_ext(i_val[1])
                except:
                    print("Invalid filename.")

                datasock = create_datasock(commsock)
                port_str = construct_port(datasock.getsockname()[1])
                port_cmd = (
                    f"PORT {datasock.getsockname()[0].replace('.', ',')},{port_str}\r\n"
                )
                commsock.sendall(port_cmd.encode("ascii"))
                print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())

                if file_ext in ASCII_EXT:
                    commsock.sendall(f"TYPE A\r\n".encode("ascii"))
                    print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())
                    commsock.sendall(f"STOR {i_val[1]}\r\n".encode("ascii"))
                    resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                    print(resp)
                    if resp[0:3] == "150":
                        dataconn, dataddr = datasock.accept()
                        with open(i_val[1], "r") as input_file:
                            data = input_file.read(BUFF_SIZE)
                            while data:
                                dataconn.sendall(data.encode("ascii"))
                                data = input_file.read(BUFF_SIZE)
                            dataconn.close()
                            resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                            print(resp)
                    else:
                        print("REQUESTED FILE UNAVAILABLE")
                else:
                    commsock.sendall(f"TYPE I\r\n".encode("ascii"))
                    print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())
                    commsock.sendall(f"STOR {i_val[1]}\r\n".encode("ascii"))
                    resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                    print(resp)
                    if resp[0:3] == "150":
                        dataconn, dataddr = datasock.accept()
                        with open(i_val[1], "rb") as input_file:
                            data = input_file.read(BUFF_SIZE)
                            while data:
                                dataconn.sendall(data)
                                data = input_file.read(BUFF_SIZE)
                            dataconn.close()
                            resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                            print(resp)
                    else:
                        print("REQUESTED FILE UNAVAILABLE")
                datasock.close()
            elif ftp_msg == "RETR":
                try:
                    file_ext = get_file_ext(i_val[1])
                except:
                    print("Invalid filename.")

                datasock = create_datasock(commsock)
                port_str = construct_port(datasock.getsockname()[1])
                port_cmd = (
                    f"PORT {datasock.getsockname()[0].replace('.', ',')},{port_str}\r\n"
                )
                commsock.sendall(port_cmd.encode("ascii"))
                print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())

                if file_ext in ASCII_EXT:
                    print("ascii")
                    commsock.sendall(f"TYPE A\r\n".encode("ascii"))
                    print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())
                    commsock.sendall(f"RETR {i_val[1]}\r\n".encode("ascii"))
                    resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                    print(resp)
                    if resp[0:3] == "150":
                        dataconn, dataddr = datasock.accept()
                        with open(
                            os.getcwd() + "/downloads/" + i_val[1], "w"
                        ) as output_file:
                            data = dataconn.recv(BUFF_SIZE)
                            while data:
                                output_file.write(data.decode())
                                data = dataconn.recv(BUFF_SIZE)
                            dataconn.close()
                            resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                            print(resp)
                    else:
                        print("REQUESTED FILE UNAVAILABLE")
                else:
                    commsock.sendall(f"TYPE I\r\n".encode("ascii"))
                    print(commsock.recv(BUFF_SIZE).decode("ascii").rstrip())
                    commsock.sendall(f"RETR {i_val[1]}\r\n".encode("ascii"))
                    resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                    print(resp)
                    if resp[0:3] == "150":
                        dataconn, dataddr = datasock.accept()
                        with open(
                            os.getcwd() + "/downloads/" + i_val[1], "wb"
                        ) as output_file:
                            data = dataconn.recv(BUFF_SIZE)
                            while data:
                                output_file.write(data)
                                data = dataconn.recv(BUFF_SIZE)
                            dataconn.close()
                            resp = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
                            print(resp)
                    else:
                        print("REQUESTED FILE UNAVAILABLE")
                datasock.close()
            else:
                print("Unknown command.")

        except KeyError:
            print("Not a valid input.")


def comm_recv(commsock):
    while RUN == 1:
        data = commsock.recv(BUFF_SIZE).decode("ascii").rstrip()
        if not data:
            break
        print(data.decode("ascii").rstrip())


def data_func(conn):
    while RUN == 1:
        data = conn.recv(BUFF_SIZE)
        if not data:
            break
        print(data.decode("ascii").rstrip())


def main():
    """
    Main function
    """
    args = parse_args()
    if ":" in args.host_port:
        host_port = args.host_port.split(":")
    else:
        host_port = [args.host_port, "21"]

    # print(host_port)

    commsock = setup_tcp(host_port)
    # port_string = construct_port(datasock.getsockname()[1])
    # print(port_string)
    # conn, addr = datasock.accept()
    try:
        comm_func(commsock, args)
    except KeyboardInterrupt:
        RUN = 0
        commsock.close()
    # with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    #     try:
    #         executor.submit(comm_func, commsock, datasock, port_string, args)
    #         # executor.submit(data_func, conn)
    #         # executor.submit(comm_recv, commsock)
    #     except KeyboardInterrupt:
    #         RUN = 0
    #         print("Closing threads")
    #         commsock.shutdown()
    #         commsock.close()
    #         executor._threads.clear()
    #         concurrent.futures.thread._threads_queues.clear()


if __name__ == "__main__":
    main()
