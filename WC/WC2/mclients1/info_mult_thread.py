#! /usr/bin/env python3

# * Imports
import socket
import time
import sys
import threading


# * Class
class ThreadedServer():
    """
    Threaded server class
    """

    def __init__(self, host, port, timeout):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.data = ""
        self.server_socket = 0
        self.stop = threading.Event()

    def setup_server(self):
        """
        Function to setup server
        """
        try:
            self.server_socket = socket.socket(socket.AF_INET,
                                               socket.SOCK_STREAM)
        except OSError:
            self.server_socket = None
        try:
            self.server_socket.setsockopt(socket.SOL_SOCKET,
                                          socket.SO_REUSEADDR, 1)
            self.server_socket.bind((self.host, self.port))
            self.server_socket.listen()
        except OSError:
            self.server_socket.close()
            self.server_socket = None

        if self.server_socket is None:
            print("Could not create socket")
            sys.exit(1)

    def parse_commands(self, cmd, addr):
        """
        Function to parse incoming commands
        """
        if cmd == "IP":
            return str(addr[0])
        elif cmd == "TIME":
            return str(time.ctime())
        elif cmd == "EXIT":
            self.stop.set()
            tmp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            tmp_socket.connect(('localhost', 2000))
            return "CLOSING SERVER"
        else:
            return "UNKOWN COMMAND"

    def client_listen(self):
        """
        Function to listen to incomming connections
        """
        try:
            conn, addr = self.server_socket.accept()
            conn.settimeout(self.timeout)
            # new_thread = threading.Thread(target=self.handle_client,
            #                               args=(conn, addr),
            #                               daemon=True)

            new_thread = threading.Thread(target=self.handle_client,
                                          args=(conn, addr))
            new_thread.start()
        except OSError:
            print("Could not accept new connection")

    def handle_client(self, conn, addr):
        """
        Function to handle client requests
        """
        reply = ""
        indata = ""

        while not self.stop.is_set():
            try:
                data = conn.recv(1).decode("utf-8")
            except socket.timeout:
                print("Timeout")
                conn.close()
                break
            except OSError:
                print("Could not receive data")
                conn.close()
                break

            if not data:
                print("Lost connection")
                break

            indata += data
            if "\n" in indata:
                reply = self.parse_commands(indata.replace("\n", "")
                                            .replace("\r", ""),
                                            addr)
                indata = ""
                try:
                    conn.sendall(reply.encode("utf-8"))
                except OSError:
                    print("Could not send data.")
                    conn.close()
                    break

        conn.close()

    def run_loop(self):
        """
        Loop to run telnet server
        """
        self.setup_server()
        try:
            while not self.stop.is_set():
                self.client_listen()
        except (KeyboardInterrupt, SystemExit):
            self.stop.set()
            print("Exiting...")

        self.server_socket.shutdown(socket.SHUT_RDWR)
        self.server_socket.close()


# * MAIN
def main():
    """
    Main function
    """
    server_test = ThreadedServer("127.0.0.1", 2000, 30)
    server_test.run_loop()

    print("Server terminated.")


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Exiting...")

    sys.exit(0)
