#! /usr/bin/env python3

# * Imports
import socket
import time
import sys
import select


# * Class
class SelectServer():
    """
    Select server class
    """

    def __init__(self, host, port, timeout):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.server_socket = 0
        self.listen_list = []
        self.stopflag = 0
        self.conn_data_dict = {}
        self.readtime_dict = {}
        self.setup_server()

    def setup_server(self):
        """
        Function to setup server
        """
        try:
            self.server_socket = socket.socket(socket.AF_INET,
                                               socket.SOCK_STREAM)
        except OSError:
            self.server_socket = None
        try:
            self.server_socket.setsockopt(socket.SOL_SOCKET,
                                          socket.SO_REUSEADDR, 1)
            self.server_socket.bind((self.host, self.port))
            self.server_socket.settimeout(3)
            self.server_socket.listen()
            self.server_socket.setblocking(False)
        except OSError:
            self.server_socket.close()
            self.server_socket = None

        if self.server_socket is None:
            print("Could not create socket")
            sys.exit(1)

        self.listen_list.append(self.server_socket)

    def parse_commands(self, cmd, addr):
        """
        Function to parse incoming commands
        """
        if cmd == "IP":
            return str(addr[0])
        elif cmd == "TIME":
            return str(time.ctime())
        elif cmd == "EXIT":
            self.stopflag = 1
            # tmp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # tmp_socket.connect(('localhost', 2000))
            return "CLOSING SERVER"
        else:
            return "UNKOWN COMMAND"

    def pop_sock(self, sock):
        """
        Pop socket from listen list and conn dict
        """
        print(sock)
        self.listen_list.remove(sock)
        self.conn_data_dict.pop(sock.getpeername(), None)
        self.readtime_dict.pop(sock.getpeername(), None)

    def handle_receive(self, sock):
        """
        Handle incomming messages
        """
        dict_key = sock.getpeername()
        try:
            data = sock.recv(1).decode("utf-8")
            #self.conn_data_dict[dict_key] += sock.recv(1).decode("utf-8")
        except socket.timeout:
            print("Timeout")
            self.pop_sock(sock)
            sock.close()
            return
        except OSError:
            print("Could not receive data")
            self.pop_sock(sock)
            sock.close()
            return

        if not data:
            print("Lost connection")
            self.pop_sock(sock)
            sock.close()
            return

        self.conn_data_dict[dict_key] += data

        if "\n" in self.conn_data_dict[dict_key]:
            indata = self.conn_data_dict[dict_key]
            reply = self.parse_commands(indata.replace("\n", "")
                                        .replace("\r", ""),
                                        dict_key)
            self.conn_data_dict[dict_key] = ""
            try:
                sock.sendall(reply.encode("utf-8"))
            except OSError:
                print("Could not send data.")
                self.pop_sock(sock)
                sock.close()
                return

    def run_loop(self):
        """
        Run function
        """
        
        while not self.stopflag:
            try:
                readable, _, exceptionable = select.select(self.listen_list,
                                                           [],
                                                           self.listen_list, 1)
            except TimeoutError:
                print("Timeout")
            except select.error:
                for sock in readable:
                    sock.shutdown(2)    # 0 = done receiving, 1 = done sending, 2 = both
                    sock.close()
                # connection error event here, maybe reconnect
                print("Connection error")
                break
            now = time.time()

            for sock in readable:
                if sock is self.server_socket:
                    conn, addr = self.server_socket.accept()
                    conn.setblocking(False)
                    conn.settimeout(self.timeout)
                    self.conn_data_dict[addr] = ""
                    self.readtime_dict[conn] = now
                    self.listen_list.append(conn)
                else:
                    self.readtime_dict[sock] = now
                    self.handle_receive(sock)

            for sock in exceptionable:
                print("Select exception")
                self.pop_sock(sock)
                sock.close()

            closed_list = []

            for sock in self.readtime_dict:
                if sock not in readable and now - self.readtime_dict[sock] > self.timeout:
                    print(f"Timeout {sock}")
                    self.pop_sock(sock)
                    sock.close()
                    closed_list.append(sock)

            for sock in closed_list:
                del self.readtime_dict[sock]

# * Main
def main():
    """
    Main function
    """
    try:
        server_test = SelectServer("127.0.0.1", 2000, 30)
        server_test.run_loop()

        server_test.server_socket.shutdown(socket.SHUT_RDWR)
        server_test.server_socket.close()
    except KeyboardInterrupt:
        print("Exiting...")

    print("Server terminated.")


if __name__ == "__main__":
    main()
