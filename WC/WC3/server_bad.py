#! /usr/bin/env python3

import socket
import sys
import re
import time


UDP_IP = "127.0.0.1"
UDP_PORT = 2000

try:
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((UDP_IP, UDP_PORT))
    except OSError:
        print("Could not set up server.")
        sys.exit(1)

    input_pattern = re.compile(r"^(PING-\d+!)$")

    while True:
        data, addr = sock.recvfrom(1024)
        data_d = data.decode("utf8")
        send_time = time.perf_counter()
        if input_pattern.match(data_d):
            ping_num = int(re.search(r"\d+", data_d).group())
            while (time.perf_counter() - send_time) < 1.0:
                print("sending")
                sock.sendto(f"POONG-{ping_num}!".encode("utf8"), addr)
                time.sleep(0.01)
except KeyboardInterrupt:
    sock.close()
    print("Exiting...")
    sys.exit(0)
