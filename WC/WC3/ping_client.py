#! /usr/bin/env python3

import socket
import sys
import time
import re
from functools import reduce


UDP_PORT = 2000
TIMEOUT = 1.0


def check_args():
    """
    Check command line arguments
    """
    ping_amount = 4
    if len(sys.argv) < 2:
        print(f"Error: run with argument IP (e.g. {sys.argv[0]} 127.0.0.1)")
        sys.exit(1)
    if len(sys.argv) > 2:
        ping_amount = int(sys.argv[2])
    return ping_amount


def setup_server():
    """
    Function to set up the server
    """
    try:
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # server_sock.setblocking(False)
        server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # server_sock.settimeout(1.0)
    except OSError:
        print("Could not set up socket.")
        sys.exit(1)

    return server_sock


def run_ping(server_sock, ping_amount):
    """
    Run the ping commands
    """
    actual_ping = 0
    total_timeout = 0
    avg_rtt_list = []
    try:
        for _ in range(ping_amount):
            if actual_ping == 1000:
                actual_ping = 0

            send_time = time.perf_counter()
            server_sock.settimeout(TIMEOUT)
            server_sock.sendto(
                f"PING-{actual_ping}!".encode("utf8"), (sys.argv[1], UDP_PORT)
            )
            data_d = ""
            try:
                while data_d != f"PONG-{actual_ping}!":
                    server_sock.settimeout(TIMEOUT - (time.perf_counter() - send_time))
                    data, _ = server_sock.recvfrom(1024)
                    data_d = data.decode("utf8")
            except OSError:
                print(f"{actual_ping}: timeout")
                total_timeout += 1
            else:
                rtt_time = (time.perf_counter() - send_time) * 1000
                avg_rtt_list.append(rtt_time)
                print(f"{actual_ping}: {rtt_time:0.0f}ms")
            actual_ping += 1
            time.sleep(0.1)

    except KeyboardInterrupt:
        print("Exiting...")
    try:
        avg_rtt = int(reduce(lambda x, y: x + y, avg_rtt_list) / len(avg_rtt_list))
    except TypeError:
        avg_rtt = 0
    print(f"AVG={avg_rtt} TIMEOUTS={total_timeout}")


if __name__ == "__main__":
    with setup_server() as sock:
        run_ping(sock, check_args())
