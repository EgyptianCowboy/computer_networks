% Created 2019-11-02 Sat 21:29
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\setcounter{secnumdepth}{3}
\date{\today}
\title{Chapter 5: The Network Layer: Control Plane}
\hypersetup{
 pdfauthor={},
 pdftitle={Chapter 5: The Network Layer: Control Plane},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.0.50 (Org mode 9.2.6)}, 
 pdflang={English}}
\begin{document}

\maketitle

\section{Introduction}
\label{sec:orgdec3cb9}

\begin{itemize}
\item Per-router control: OSPF and BGP protocols.
\item Logically centralized control: besides IP forwarding other functions (load sharing, firewalling and NAT).
\end{itemize}

\section{Routing algorithms}
\label{sec:orgd7f89ba}

Compute path with least cost. A graph is used to formulate routing problems. \(G=(N,E)\) A graph is a N of nodes and a collection E of edges, where each edge is a pair of nodes from N.

A node \emph{y} is said to be a neighbor of node \emph{x} if \emph{(x,y)} belongs to \emph{E}.

A path in a graph \(G=(N,E)\) is a sequence of nodes such that each of the pairs are edges in \emph{E}. The cost of the path is simply the sum of all the edges along of the path. Given any two nodes, there are many different paths. One or more of these paths is a least-cost path. If all edges in the graph have the same cost, the least cost path is also the shortest path (with smallest number of links between source and destination).

\begin{itemize}
\item Centralized routing algorithm: Computes the least-cost path between a source and destination using complete, global knowledge about the network. The calculation itself can be run at one site or could be replicated in the routing component of each and every router. Often referred to as link-state (LS) algorithm,
\item Decentralized routing algorithm: is carried out in an iterative, distributed manner by the routers. No node has complete information about the costs of all network links. Instead each node begins with only the knowledge of the costs of its own directly attached links. One of these algorithms is called a distance-vector (DV) algorithm.
\end{itemize}

A second way to classify routing algorithms is according to whether they are static or dynamic. In static routing algorithms, routes change very slowly over time (often as a result of human intervention). Dynamic routing algorithms change the routing paths as the network traffic or topology changes (can be run periodically or in direct response to topology or link cost changes).

A third way is according to load-insensitive of load-sensitive. Load-sensitive algorithms change link costs to reflect the current level of congestion in the underlying link. Today's internet routing algorithms (RIP, OSPF and BGP) are load-insensitive (because a number of difficulties were encountered).

\subsection{Link-State routing algorithm}
\label{sec:org0371f4d}

The OSPF routing protocol uses a link-state broadcast algorithm.
Uses Dijkstra's algorithm.

If cost of two edges are equal, arbitrarily choose one.

Complexity: \emph{O(n\textsuperscript{2})}.

How to prevent oscillations as a result of traffic injection? Example: mandate that link costs not depend on the amount of traffic carried, this is unacceptable. Another solution is to ensure that not all routers run the LS algorithm at the same time. But researchers have found that routers can self-synchronize among themselves. Avoid such synchronization by randomizing the time it sends out a link advertisement.

\subsection{Distance-vector routing algorithm}
\label{sec:org7cb5367}

\(d_x(y) = min_v\{c(x,v)+d_v(y)\}\) where \emph{d\textsubscript{x}(y)} is the cost of the least-cost path from node x to y. The least costs are related by the Bellman-Ford equation.

The solution to the equation provides the entries in node \emph{x}'s forwarding table. Another contribution of the equation is that it suggests the form of the neighbor-to-neighbor communication that will take place in the DV algorithm.

The basic idea is that each node \emph{x} begin with \emph{D\textsubscript{x}(y)}, an estimate of the cost of the least-cost path from itself to node y, for all nodes y in \emph{N}. Let \(D_x = [D_x(y):\ y\ in\ N\) be node \emph{x}'s distance vector, which is the vector of cost estimates from \emph{x} to all other nodes, y, in \emph{N}. With the DV algorithm, each node x maintains the following routing information:

\begin{itemize}
\item For each neighbor v, the cost c(x,v) from x directly attached neighbor, v.
\item Node \emph{x}'s distance vector, that is \(D_x = [D_x(y):\ y\ in\ N]\), containing \emph{x}'s estimate of its cost to all destinations, y, in \emph{N}.
\item The distance vectors of each of its neighbors, \(D_v = [D_v(y):\ y\ in\ N]\), for each neighbor v of x.
\end{itemize}

In the distributed, asynchronous algorithms, from time to time, each node sends a copy of its distance vector to each neighbor. When a node, \emph{x}, receives a new distance vector from any of its neighbors \emph{w}, it saves it and then uses the Bellman-Ford equation to update its own distance vector as follows:

\begin{align*}
D_x(y) = min_v\{c(x,v+D_v(y)\} \text{for each node y in N}
\end{align*}

If node \emph{x}'s distance vector has changed as a result of this update step, node x will then send its updated distance vector to each of its neighbors, which can in turn update their own distance vectors. Miraculously enough, as long as all the nodes continue to exchange their distance vectors in an asynchronous fashion, each cost estimate D\textsubscript{x}(y) converges to d\textsubscript{x}(y), the actual cost of the least-cost path from node \emph{x} to node \emph{y}.


In the DV algorithm, a node updates its DV estimate when it either sees a cost change in one of its directly attached links or receives a DV update from a neighbor. But to update its own forwarding table for a given destination, the node really needs to know not the shortest-path distance to \emph{y} but instead the neighboring node \emph{v*(y)} that is the next-hop router along the shortest path to \emph{y}.

DV-like algorithms are used in many routing protocols, including the internet's RIP and BGP, ISO IDRP, Novell IPX and the original ARPAnet.

\subsubsection{Distance-Vector Algorithm: Link-cost changes and link failure}
\label{sec:org9a9cbf0}

The DV algorithm causes the following sequence of events to occur when a link cost decrease occurs:
\begin{itemize}
\item At time \emph{t\textsubscript{0}}, \emph{y} detects a link-cost change. \emph{y} updates its distance vector and informs its neighbors.
\item At time \emph{t\textsubscript{1}}, \emph{z} receives the update. It somputes a new least cost to \emph{x} and sens its new distance vector to its neighbors.
\item At time \emph{t\textsubscript{2}}, \emph{y} receives \emph{z}'s update and updates its distance table. \emph{y}'s least costs do not change and hence \emph{y} does not send any messages. The algorithms comes to a quiescent state.
\end{itemize}

Thus only two iterations are required to reach a quiescent state. It has propagated quickly.

For link cost increase, see page 417. \textbf{Routing loop} if routing tables are not updated fast enough.

\subsubsection{Distance-vector algorithm: adding poisoned reverse}
\label{sec:org3e830bf}

The previous looping problem can be avoided with \emph{poisoned reverse}. If \emph{z} routes through \emph{y} to get to destination \emph{x}, then \emph{z} will advertise to \emph{y} that its distance to \emph{x} is infinity, that is. \emph{z} will advertise to \emph{y} that \(D_z(x) = \infty\). \emph{z} will continue telling this white lie to \emph{y} as long as it routes to \emph{x} via \emph{y}. Since \emph{y} believes that \emph{z} has no path \emph{x}, \emph{y} will never attempt to route to \emph{x} via \emph{z}, as long as \emph{z} continues to route to \emph{x} via \emph{y}.

Loops that involve three or more loops will not be detected by the poisoned reverse technique.

\subsubsection{A comparison of LS and DV routing algorithms}
\label{sec:orgc683530}

DV and LS take complementary approaches. Comparison:
\begin{itemize}
\item Message complexity: LS requires each node to know the cost of each link in the network. Whenever a link cost changes, the new link cost must be sent to all nodes. DV requires message exchanges between directly connected neighbors at each iteration.
\item Speed of convergence: LS is an \emph{O(N\textsuperscript{2})} algorithm requiring \emph{O(NE)} messages. DV can converge slowly and can have routing loops while the algorithm is converging. DV also suffers from the count-to-infinity problem.
\item Robustness: Under LS, a router could broadcast an incorrect cost of one of its attached links. A node could also corrupt or drop any packets it received as part of an LS broadcast. But an LS node is computing only its own forwarding tables, other nodes perform similar calculations for themselves. Route calculations are somewhat separated under LS. Under DV, a node can advertise incorrect least-cost paths to any or all destinations. At each iteration, a node's calculation in DV is passed on to its neighbor and then indirectly to its neighbor's neighbor etc. In this sense, an incorrect node calculation can be diffused through the entire network under DV.,
\end{itemize}


\section{Intra-AS routing in the internet: OSPF}
\label{sec:orgf90dbcf}

In practice, this model and its view of a homogenous set of routers all executing the same routing algorithm is simplistic for two important reasons:
\begin{itemize}
\item Scale: as the number of routers grow, the overhead in communication, computing and storing information becomes prohibitive. DV algorithms that would never converge. Route computation complexity must be reduced.
\item Administrative autonomy: an organization should be able to operate and administer its network as it wished, while still being able to connect its network to other outside networks.
\end{itemize}

Both can be solved by organizing routers into autonomous systems (ASs). Ofter routers in an ISP, and routers that interconnect them, constitute a single AS. Some ISPs partition their network into multiple ASs.

Routing algorithms running within an AS is called an intra-autonomous system routing protocol.

\subsection{Open shortest path first (OSPF), RFC 2328}
\label{sec:org7a15223}

Closely related to IS-IS, they are widely used for intra-AS routing in the internet.

It is a link-state protocol that uses flooding of link-state information and a Dijkstra's least-cost path algorithm. With OSPF, each router constructs a complete topological map of the entire autonomous system. Each router then locally runs Dijkstra's shortest-path algorithm to determine a shortest-path tree to all subnet, with itself as the root node. Link costs are set by the administrator. They might be set to all 1 to achieve minimum-hop routing or set to be inversely proportional to the link capacity in order to discourage traffic from using low-bandwidth links. OSPF does not mandate a policy for how link weights are set, but providdes the mechanisms for determining least-cost path routing for the given set of link weights.

A router broadcasts routing info to all other routers, not just neighbors. It also checks that links are operational and allows an OSPF router to obtain a neighbor's database of network-wide link state.

Some advances (see page 422):
\begin{itemize}
\item Security
\item Multiple same-cost paths
\item Integrated support for unicast and multicast routing
\item Support for hierarchy withing a single AS
\end{itemize}

\section{Routing among ISPs: BGP}
\label{sec:org20a7ceb}

We need an inter-autonomous system routing protocol. All ASs run the same inter-AS protocol called Border Gateway Protocol (BGP), RFC 4271. BGP is a decentralized and asynchronous protocol in the vein of DV routing.

\subsection{The role of BGP}
\label{sec:org26e87ef}

In BGP, packets are not routed to specific destination addresses, but instead to CIDRized prefixes. Each prefix representing a subnet or a collection of subnets.

BGP provides each router a means to:
\begin{itemize}
\item Obtain prefix reachability information from neighboring ASs. BGP allows each subnet to advertise its existence to the rest of the internet.
\item Determine the "best" routes to the prefixes. A router may learn about two or more different routes to a specific prefix. To determine the best route, the router will locally run a BGP route-selection procedure.
\end{itemize}

\subsection{Advertising BGP route information}
\label{sec:orgbc5acd5}

For each AS, each router is either a gateway router or an internal router. Internal routers connect only to hosts and routers within its own AS.

Pairs of routers exchange routing information over semi-permanent TCP connections using port 179. Each such TCP connection, along with all the BGP messages sent over connection is called a BGP connection. A BGP connection that spans two ASs is called an external BGP (eBGP) connection. A BGP session between routers in the same AS is called an internal BGP (iBGP).

In order to propagate reachability information, both iBGP and eBGP sessions are used.

\subsection{Determining the best routes}
\label{sec:orgfdde7a4}

When a router advertises a prefix across a BGP connection, it includes with the prefix several BGP attributes. A prefix with its attributes is called a route. Two important attributes are AS-PATH and NEXT-HOP. AS-PATH contains the list of ASs through which the advertisement has passed, when a prefix passes through an AS, the AS adds its ASN to the list in the AS-PATH. 

NEXT-HOP is the IP address of the router interface that begins the AS-PATH.

\subsubsection{Hot potato routing}
\label{sec:orgb4a09cf}

The BGP routing algorithm is called hot potato routing.

In hot potato routing, the route chosen is the route with the least cost to the NEXT-HOP router beginning that route. It is important to note that when adding an outside-AS prefix into a forwarding table, both the inter-AS routing protocol and the intra-AS routing protocol are used.

The idea behind this algorithm is to get packets out of its AS as quickly as possible without worrying about the cost of the remaining portions of the path outside of its AS to the destination. It is thus a selfish algorithm, it tries to reduce the cost to its own AS while ignoring the other components of the end-to-end costs outside of its AS.

\begin{figure}[!htpb]
\centering
\includegraphics[width=.9\linewidth]{hpr_1.jpg}
\caption{Steps in adding outside-AS destination in a router's forwarding table}
\end{figure}

\subsubsection{Route-selection algorithm}
\label{sec:org26ca426}

BGP sequentially invokes the following elemination rules until one route remains:
\begin{itemize}
\item A route is assigned a local preference value as one of its attributes. This preference could have been set by the router, learned from another router in the same AS. Its policy decision is left up to the AS's network admin. The highest preference is selected
\item From the remaining routes the shortest AS-PATH is selected.
\item From the remaining routes, hot potato routing is used, that is, the route with the closest NEXT-HOP router.
\item If still more than one route remains, BGP identifiers are used to select a route.
\end{itemize}

\subsubsection{IP-anycast (RFC 1546)}
\label{sec:org2ba3bd8}

IP-anycast is commonly used in DNS. IP-anycast interesting for:
\begin{itemize}
\item Replicating the same content on different servers in many dispersed geographical locations.
\item Having each user access the content from the server that is closest.
\end{itemize}

For example: CDN, DNS to replicate DNS records on servers. When a user wants to access this replicated content, it is desirable to point the user to the nearest server with replicated content. BGP's route-selection algorithm preovides an easy and natural mechanism for doing so.

During the IP-anycast configuration stage, the CDN company assigns the same IP address to each of its servers. When a BGP router receives multiple route advertisements for this IP, it treats these advertisements as providing different paths to the same physical location. When configuring its routing table, each router will locally use the BGP route-selection algorithm to pick the best route to that IP address. After this initial BGP address-advertisement phase, the CDN can distribute its content.

In practice CDNs generally choose not to use IP-anycast because BGP routing changes can result in different packets of the same TCP connection arriving adt different instances of the web server. But it is still used extensively by DNS to direct queries to the closest root DNS server.

\subsubsection{Routing policy}
\label{sec:orgcf0c8a0}

When a router selects a route, the AS routing policy trumps all other consideration, such as shortest AS path or hot potato routing.

Multi-homed access ISP, connected to the rest of the network via two providers. How can X be forced to only be source/destination of all traffic leaving/entering X? By controlling the manner in which BGP routes are advertised. X will function as an access ISP network if it advertises that is has no paths to any other destination except itself.

Consider provider networks. Should provider B burden the traffic/cost of carrying traffic from customers of other providers? General rule of thumb: any traffic flowing across an ISP's backbone network must either have a source or destination in a network that is a customer of that ISP.

\subsubsection{Putting the pieces together: obtaining internet presence}
\label{sec:orgcd2a744}
\subsection{The SDN control plane}
\label{sec:orgbd792e5}

Four key characteristics (see page 435):
\begin{itemize}
\item Flow based forwarding.
\item Separation of data plane and control plane.
\item Network control functions: external to data-plane switches.
\item A programmable network.
\end{itemize}

SDN represents a significant unbundling of network functionality: data plane switches, SDN controllers and network-control applications are separate entities.

\subsubsection{The SDN control plane: SDN controller and SDN network-control applications}
\label{sec:orgba4f724}

SDN control plane divides into two components: SDN controller and the SDN network-control applications.

Layers (page 438):
\begin{itemize}
\item A communication layer: communicating between the SDN controller and controlled network devices.
\item A network-wide state-management layer. To make control decisions, e.g., configuring flow tables in all switches, implement load balancing, implement firewalling capabilities.
\item The interface to the network-control application layer. Northbound interface. Allows network-control applications to read/write network state and flow tables.
\end{itemize}

\subsubsection{OpenFlow protocol}
\label{sec:orgcae063c}

Operates between an SDN controller and an SDN-controlled switch or other device.

Important messages from controller to controlled switch(page 440):
\begin{itemize}
\item Configuration.
\item Modify-state.
\item Read-state.
\item Send-packet.
\end{itemize}

Important messages from controlled switch:
\begin{itemize}
\item Flow-removed. Flow table entry has been removed.
\item Port status. Change in port status.
\item Packet-in. Packet does not match any flow table entry.
\end{itemize}


\subsubsection{Data and control plane interaction: example (page 442)}
\label{sec:org3867166}
\subsubsection{SDN: past and future}
\label{sec:orgb7ebd74}
\section{ICMP: The Internet Control Message Protocol (RFC 792)}
\label{sec:orgebdde10}

Used by hosts and routers to communicate network-layer information to each other. Lies just above IP, carried inside IP datagrams, as an IP payload. When as host receives an IP datagram with ICMP specified as the upper-layer protocol (1), it demultiplexes the datagram's contents to ICMP, just like TCP/UDP.

ICMP messages have a type and code field, header is 8 bytes. Not only used for signaling error conditions.

Ping command sends ICMP type 8 code 0 message to the specified host, host sends back type 0 code 0 ICMP echo reply.

Another interesting ICMP message is the source quench message. Original purpose was to perform congestion control, allow congested routers to send an ICMP source quench message to a host to force that host to reduce its transmission rate.

Traceroute is also implemented using ICMP messages. Traceroute in the source sends a series of ordinary IP datagrams to the destination. Each of these datagrams carries a UDP segment with an unlikely UDP port number. The first of these datagrams has a TTL of 1, the second of 2, the third of 3, and so on. The source also starts timers for each of the datagrams. When the nth datagram arrives at the nth router, the nth router observes that the TTL of the datagram has just expired. According to the rules of the IP protocol, the router discards the datagram and sends an ICMP warning message to the source (type 11 code 0). This warning message includes the name of the router and its IP address. When this ICMP message arrives back at the source, the source obtains the round-trip time from the timer and the name and IP address of the nth router from the ICMP message. The source knows it has reached the destination when the host sends a port unreachable ICMP message (type 3 code 3) back to the source.

\section{Network management and SNMP}
\label{sec:org000843b}

\subsection{Network management framework}
\label{sec:orgae5c1ab}
Key components (page 450):
\begin{itemize}
\item Managing server.
\item Managed device is a piece of network equipment that resides on a managed network. May be several managed objects in a managed device.
\item Management Information Base (MIB), each managed object within a managed device associated info is collected in an MIB.
\item Network management agent, a process running in the managed device that communicates with the managing server.
\item Network management protocol. Runs between the managing server and the managed devices, allowing the server to query the status of managed devices and indirectly take actions at these devices via its agents. Example SNMP (simple network management protocol)
\end{itemize}

\subsubsection{SNMP (RFC 3416)}
\label{sec:orgb8bf458}

Is an application layer protocol used to convey network-management control and info messages between managing server and an agent executing on behalf of that server. Most common usage is in a request-response mode. A second common usage is for an agent to send unsolicited messages. Known as trap messages to notify the server of an exceptional situation.

SNMPv2 defines seven types of messages.

\begin{figure}[!htpb]
\centering
\includegraphics[width=.9\linewidth]{snmp.jpg}
\caption{SNMP PDU types}
\end{figure}
\end{document}
