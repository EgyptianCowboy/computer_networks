(TeX-add-style-hook
 "HC9"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org9451126"
    "sec:org51735ce"
    "sec:orgaa01735"
    "sec:org69470f9"
    "sec:org193ea63"
    "sec:orgee81c16"
    "sec:org6a16a42"
    "sec:org1db102c"
    "sec:org4e23c91"
    "sec:org23ddc4d"
    "sec:org8cd7591"
    "sec:org1e21baf"
    "sec:org931722f"
    "sec:org6260ab0"
    "sec:orgc409f22"))
 :latex)

