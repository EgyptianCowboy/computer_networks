#+OPTIONS: H:3 num:3
#+OPTIONS: toc:nil
#+TITLE: Chapter 3: Transport Layer
#+STARTUP: align
#+OPTIONS: ^:nil

* Preparation
See [[file:~/Documents/school/uhasselt/computer_networks/HC/HC4/HC4.org][HC4]].
* Lesson
** Selective repeat
Receiver/sender do not be to be in sync.

Sequence number too high, causes: receive window in sender wrong.

If sequence number is in receive window size before receive window, send explicit ACK to sender (receiver -> sender ACK lost).

$sequenceNumber = 2 \cdot windowSize$

** TCP
Reliable, in-order byte stream. Pipelined, point-to-point, full duplex.

Sequence number is byte number because the receiver can choose how many bytes it wants to take out of the buffer, how much bytes to handle. Useful to 'clock' the connection, sender and receiver can work at different speeds.

TCP has cumulative ACKs, but does not use GBN (selective ACKs [SACK]), option header SACK permitted.

*** Telnet
*Nagle's Algorithm*

Wacht tot X bytes zijn geaccumuleerd voordat deze naar de telnet server worden gestuurd.

Remote mouse movements: TCP_NODELAY.

*** RTT
RTT wordt continu gemeten.

Eerste RTT meting bij handshake.

EstimatedRTT is een exponential weighted moving average.

$EstimatedRTT = (1 - \alpha) \cdot EstimatedRTT + \alpha \cdot SampleRTT$

Ook wordt er een DevRTT berekend (deviation of RTT).

*** Reliable ACK

TCP ACK generation [RFC 1122, RFC 2581]

| event at receiver           | TCP receiver action                                      |
|-----------------------------+----------------------------------------------------------|
| Arrival of in order segment | Wait up to 500 ms for next segment (only next, not more) |
| (see sheets for full table) |                                                          |
|                             |                                                          |
|                             |                                                          |

*Delayed ACKs + Nagle's algorithm*

Delayed ACK algorithm should be used by a receiver. A TCP receiver should not excessively delay ACKs. An ACK should be generated for at least every second full-sized segment.

Most TCP implementations support SACKs, mix of GBN and SR.

*** Fast retransmit

Retransmit a segment before the timer runs out after the sender receive 3 duplicate ACKs.

*** Flow control

*Silly window syndrome*

Application reads one byte from the buffer, sends to the sender that one byte is free, sender sends one byte etc.

Sender sends large blocks, receiver reads one byte. Clark's solution: delay window size updates until enough space is free (at least one MSS/half buffer). Sender waits until a full segment can be sent (half receive buffer is free).

Clark and Nagle are complementary. Sender doesn't send small segments, receiver doesn't ask for small segments.

*** Connection establishments

Two-way handshake vs three-way handshake.

*** Congestion control

Sender has to adapt to what is going on in the network.

*** TCP congestion control

AIMD (additive increase, multiplicative decrease).

TCP slow start: for every ACK (size of ACK), send two more segments.

Two loss triggers: timeout or duplicate ACKs.

Timeout: 
- cwnd to 1 MSS.
- Set state to slow start.

Duplicate ACKs:
- Half cwnd.
- cwnd grows linearly (congestion avoidance).

Variable ssthresh (segment size threshold). This sets to 1/2 cwnd when a loss event occurs. When cwnd > ssthresh -> congestion avoidance.

TCP has a very low error tolerance when trying to achieve high throughput, new TCP version necessary.
*** Fairness

Multiple TCP connections converge to a fair distribution.
*** Explicit Congestion Notification
ECE = Excplicit Congestion Experienced.
* Ex
Reading: 4.1 - 4.4

Wireshark
